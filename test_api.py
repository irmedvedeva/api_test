import pytest
import requests
from data_to_test import DataToTest as d


def test_get_users(
    url=d.GetUsers.url, headers=d.GetUsers.headers, data=d.GetUsers.data
):
    # get users
    response = requests.request("GET", url, headers=headers, data=data)
    print(response.text.encode("utf8"))
    response = response.json()
    return response


def test_check_user(
    url=d.CheckUser.url, headers=d.CheckUser.headers, data=d.CheckUser.data
):
    # get user
    response = requests.request("GET", url, headers=headers, data=data)
    print(response.text.encode("utf8"))
    response = response.json()
    return response
    assert response.status_code == 200


def test_create_user(
    url=d.CreateUser.url, headers=d.CreateUser.headers, data=d.CreateUser.data
):
    # create user
    response = requests.request("POST", url, headers=headers, data=data)
    print(response.text.encode("utf8"))
    assert response.status_code == 200


def test_update_user(
    url=d.UpdateUser.url, headers=d.UpdateUser.headers, data=d.UpdateUser.data
):
    response = requests.request("PUT", url, headers=headers, data=data)
    print(response.text.encode("utf8"))
    assert response.status_code == 200


def test_delete_user(
    url=d.UpdateUser.url, headers=d.UpdateUser.headers, data=d.UpdateUser.data
):
    response = requests.request("DELETE", url, headers=headers, data=data)
    print(response.text.encode("utf8"))
    assert response.status_code == 200


@pytest.mark.xfail
def test_wrong_token(
    url=d.WrongToken.url, headers=d.WrongToken.headers, data=d.WrongToken.data
):
    # try wrong token
    response = requests.request("GET", url, headers=headers, data=data)
