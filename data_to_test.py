class DataToTest:
    class GetUsers:
        # -- - PARAMS--
        url = "https://gorest.co.in/public-api/users"
        data = {}
        headers = {
            "Authorization": "Bearer 0dZoAWyevo1qBY8etQB_ESJ5mGB7Slrj3bzW"
            }

    class CheckUser:
        url = "https://gorest.co.in/public-api/users/23"
        data = {}
        headers = {
            "Authorization": "Bearer 0dZoAWyevo1qBY8etQB_ESJ5mGB7Slrj3bzW"
            }

    class UpdateUser:
        url = "https://gorest.co.in/public-api/users/2323"
        data = '{"id": "2343", "first_name": "Ira"}'
        headers = {
            "Authorization": "Bearer 0dZoAWyevo1qBY8etQB_ESJ5mGB7Slrj3bzW"
            }

    class CreateUser:
        url = "https://gorest.co.in/public-api/users"
        data = '{\n"email":"h@mail.com",\n"first_name":"H",\n"last_name":"J"}'
        headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer 0dZoAWyevo1qBY8etQB_ESJ5mGB7Slrj3bzW",
            "Content-Type": "text/plain",
        }

    class WrongToken:
        url = "https://gorest.co.in/public-api/users/23"
        data = {}
        headers = {"Authorization": "Bearer zW"}
